Objective: Today, I learned what Mapper is and learned how to integrate flyway into the Spring Boot project. In the afternoon, our group gave a speech on the CI/CD theme and learned about other cloud native technologies through other groups.

Reflective:  Flyway is knowledge that I have not been exposed to, and I have learned more.

Interpretive: Flyway is like MYSQL’s log. But flyway tends to showcase through code.

Decisional: I will try to use Flyway for project database construction until I master it. I have gained a general understanding of cloud native knowledge through speeches from other groups, but more detailed content requires me to understand it offline.