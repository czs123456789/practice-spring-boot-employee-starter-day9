package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JPAEmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findAllByGender(String gender);

    @Query(value = "select e.* from employees e limit (?1-1)*?2,?2",nativeQuery = true)
    List<Employee> findByPage(Integer page, Integer size);

}
