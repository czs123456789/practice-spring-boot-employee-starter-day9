package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }


    public List<Employee> findAll() {
        return jpaEmployeeRepository.findAll();
    }

    public EmployeeResponse findById(Long id) {
        Employee employee = jpaEmployeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employee);
    }

    public void update(Long id, EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee toBeUpdatedEmployee = EmployeeMapper.toEntity(findById(id));
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return jpaEmployeeRepository.findAllByGender(gender);
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        Employee saveEmployee = jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(saveEmployee);
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return jpaEmployeeRepository.findByPage(page, size);
    }

    public void delete(Long id) {
//        jpaEmployeeRepository.delete(findById(id));
        jpaEmployeeRepository.deleteById(id);
    }
}
