package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/21/2023
 **/
public class EmployeeMapper {
    public static Employee toEntity(EmployeeRequest employeeRequest) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRequest, employee);

        return employee;
    }

    public static Employee toEntity(EmployeeResponse employeeResponse) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeResponse, employee);

        return employee;
    }

    public static EmployeeResponse toResponse(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);

        return employeeResponse;
    }

    public static EmployeeRequest toRequest(Employee employee) {
        EmployeeRequest employeeRequest = new EmployeeRequest();
        BeanUtils.copyProperties(employee, employeeRequest);

        return employeeRequest;
    }
}
