package com.afs.restapi.service.dto;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/21/2023
 **/
public class CompanyRequest {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

}
