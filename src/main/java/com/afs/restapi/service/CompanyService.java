package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private JPACompanyRepository jpaCompanyRepository;


    public CompanyService(JPACompanyRepository jpaCompanyRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return jpaCompanyRepository.findAll(PageRequest.of(page-1, size)).toList();
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        CompanyResponse response = CompanyMapper.toResponse(company);
        response.setEmployeesCount(company.getEmployees().size());
        return response;
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Company newCompany = CompanyMapper.toEntity(findById(id));
        newCompany.setName(CompanyMapper.toEntity(companyRequest).getName());
        jpaCompanyRepository.save(newCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = jpaCompanyRepository.save(CompanyMapper.toEntity(companyRequest));
        CompanyResponse response = CompanyMapper.toResponse(company);
        response.setEmployeesCount(0);
        return response;
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new).getEmployees();
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
